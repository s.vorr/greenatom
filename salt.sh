#For Minion
#!/bin/bash
wget https://bootstrap.pypa.io/get-pip.py
apt install python3-pip
pip3 install salt
mkdir -p /etc/salt
mkdir  - p /etc/salt/master.d /etc/salt/minion.d
echo 'file_roots:' > /etc/salt/master.d/roots.conf
echo '  base:' >> /etc/salt/master.d/roots.conf
echo '    - /srv/salt/base' >> /etc/salt/master.d/roots.conf
mkdir -p /srv/salt/base
echo 'master: localhost' > /etc/salt/minion.d/master.conf
salt-master -d
salt-minion -d

#For Master
#!/bin/bash
wget https://bootstrap.pypa.io/get-pip.py
apt install python3-pip -y
pip3 install salt 
mkdir -p /etc/salt
mkdir -p /etc/salt/master.d /etc/salt/minion.d
echo 'file_roots:' > /etc/salt/master.d/roots.conf
echo '  base:' >> /etc/salt/master.d/roots.conf
echo '    - /srv/salt/base' >> /etc/salt/master.d/roots.conf
mkdir -p /srv/salt/base
echo 'master: localhost' > /etc/salt/minion.d/master.conf
salt-master -d
salt-minion -d