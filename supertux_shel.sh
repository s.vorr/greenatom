mkdir games
git clone https://github.com/SuperTux/supertux.git
cd supertux/
git submodule update --init --recursive
mkdir build
cd build
sudo apt-get install libfreetype6-dev libharfbuzz-dev libfribidi-dev  gtk-doc-tools
apt-get install subversion autoconf automake jam g++ 
apt-get install libsdl1.2-dev libsdl-image1.2-dev libphysfs-dev libvorbis-dev libogg-dev libopenal-dev
apt-get install libcurl3-dev
# sudo apt-get build-dep supertux
apt-get install cmake 
sudo apt install libsdl2-dev -y
sudo apt install libsdl2-image-2.0-0
# apt-get install sdl2
# apt-get install sdl2_image
sudo apt install build-essential
sudo apt install gettext
sudo apt install autoconf 
sudo apt install codeblocks 

sudo apt install liballegro-physfs5-dev
sudo apt install vorbis-tools -y
sudo apt-get install freeglut3-dev
# sudo apt-cache search glut
# dpkg -L freeglut3-dev
sudo apt install liballegro-physfs5-dev
sudo apt install libjpeg-dev libwebp-dev libtiff5-dev libsdl2-image-dev libsdl2-image-2.0-0 -y
sudo apt-get update
sudo apt-get upgarade
sudo apt-get upgrade
sudo apt-get install libglu1-mesa-dev  mesa-common-dev
# sudo apt-get install libopenal0a libopenal-dev
sudo apt-get install libopenal1
sudo apt-get install libopenal-dev 
apt-get install libalut0 libalut-dev
sudo apt install libcurl4-openssl-dev
sudo apt install libboost-dev
sudo apt install libboost-all-dev 
sudo apt-get install boost
sudo apt-get install boost-defaults 
sudo apt-get update -y
sudo apt-get install -y libglew-dev
cd supertux/
cd build/
cmake ..
make
./supertux2 