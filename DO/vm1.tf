resource "digitalocean_droplet" "vm1" {
  image  = "ubuntu-18-04-x64"
  name   = "vm1"
  region = "fra1"
  size   = "512m"
  vpc_uuid = data.digitalocean_vpc.default-fra1.id
  ssh_keys = [
    var.ssh_fingerprint,
  ]

  connection {
    type        = "ssh"
    host        = self.ipv4_address
    user        = "root"
    private_key = file(var.pvt_key)
    timeout     = "2m"
  }
  
  provisioner "file" {
    source      = "~/.ssh/id_rsa"
    destination = "~/.ssh/id_rsa"
  }
  
  provisioner "file" {
    source      = "out/hosts.txt"
    destination = "~/hosts.txt"
  }


  provisioner "remote-exec" {
    inline = [
      "export PATH=$PATH:/usr/bin",
      "apt update",
      "chmod 400 ~/.ssh/id_rsa",
    ]
  }
}

output "ip_vm1" {
    value = "${digitalocean_droplet.vm1.ipv4_address}"
}

output "private_ip_vm1" {
    value = "${digitalocean_droplet.vm1.ipv4_address_private}"
}
